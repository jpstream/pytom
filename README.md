# Pytom

This repo is just a playground I started off using to get back in touch with Python and my financial hobbies.

The server code is inspired by the recent Tech with Tim series, reference for the code is at the github repo [here](https://github.com/techwithtim/Flask-Blog-Tutorial).

## Setup

To get started make sure you have python3 installed on your system the navigate inside of this project and create a new python virtual environment using the following command.

```bash
python3 -m venv venv
```

*Note* this creates the virtual environment inside of the venv folder, *optionally* you could generate the environment in the root of the project directory and the appropiate directories will still be ignored in git.

Now activate your environment using either of the commands depending where you generated the venv directories then install the required dependencies.

```bash
source venv/bin/activate
# source bin/activate
python -m pip install -r requirements.txt
```

## Scripts

All scripts I have and am working on should be listed here. No script is ever complete, there is always ways I can definitely improve but they all get their respective jobs done.

### `exchange_stats.py`

This is meant to be the more modular version of the original `kucoin_stats.py` file.

#### *Summary*

Currently will pull all tickers from an exchange, rank them by volume and save the top 10 to a CSV file. You can pass any exchange available on *CCXT* along with a base pair on that exchange. It will look also look for any previously saved CSV files and pull data from the last candle.

You can currently call the function from another file and run it like the example below.

```python
from exchange import *

exchange_stats('kraken', 'USDT')
exchange_stats('kraken', 'USDT', '30m', )

```

Here is a overview of the parameters the function will take, below are the default attributes to be used if none are selected ones are not present.

```python
exchange_stats(exchange_id='kucoin', base_pair='USDT', timeframe='1h', top=10)
```

  - `exchange_id`: Takes any of the supported exchanges from the [CCXT](https://github.com/ccxt/ccxt) package.
  - `base_pair`: Takes any asset that is accepted as a base trading pair on the exchange being queried.
  - `timeframe`: Any acceptable timeframe supported on the exchange passed through as a string with either a `h` or `m` delimeter on the end such as `1h` or `30m`.
  - `top`: This is an integer passed through telling the handler how many of the 'top' assets you want to pull from the exchange e.g the Top 10 or Top 100 coins. *Note* these are currently ranked by `quoteVolume` only by default, this is yet to be an adjustable parameter although you can change the value on which the dataframe is sorted by in `Line 35: exchange_stats.py`.



#### *To-Do/Plans*

  - Return a message to the user when the exchange doesn't support OHLCV data
    - Then just daily data or what is available?
  - Calculate EMA and possibly other indicators and add them into the DataFrame before saving
    - This requires recalculating the same indicators everytime for *new* data

### `plotly_graph.py`

#### *Summary*

`plotly_graph.py` simply pulls OHLCV for a ticker and displays it inside a candlestick chart.

#### *To-Do/Plans*

  - Implement logic into a reusable function that takes in the ticker symbol and exchange.
  - Add indicators overlayed on the chart (MACD, Volume, BOLL)

## Database

Database notes and guides.

### Docker Setup

1. Pull the image.

```bash
docker pull postgres:latest
```

2. Create a volume to persist data.

```bash
docker volume create postgres-pytom
```

3. Run the image.

```bash
docker run -d --name=postgres-db -p 5432:5432 -v postgres-volume:/var/lib/postgresql/data -e POSTGRES_PASSWORD=devTest postgres
```

The options for the command are explained below here.

  -d will run this container in detached mode so that it runs in the background.
  --name assigns the name “postgres13” to your container instance.
  -p will bind the PostgreSQL container port 5432 to the same port on your host machine. You’ll be able to connect to localhost:5432 using PostgreSQL clients (psql) running on your host.
  -v option bind that data folder inside the container volume (/var/lib/postgresql) to the local Docker volume (postgres-volume) you created in the previous step.
  -e sets an environment variable. In this case, the PostgreSQL root password.
  postgres is the name of the image we use to create the container.

4. Connect to the instance and create your first intial database by hand.

```bash
docker exec -it pytom-db psql -U postgres
```

Once inside of a postgres shell run the following.

```sql
CREATE DATABASE pytom;
```

### Connecting to Postgres

The logic and implementation for connecting to the db specifically is inspired and first copied from [here](https://github.com/zchtodd/open-rank-tracker/).

## Development

This section is dedicated to development specific notes.

### Requirements

To get a clean list of installed packages rather then the overpopulated list that `pip freeze` generates I've opted to use `pip-chill` instead as it produces a clean list of top level user installed dependencies. Everytime you add a new package for development run the following command and commit the `requirements.txt`.

```bash
pip-chill > requirements.txt
```