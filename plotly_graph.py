import pandas as pd
import plotly.graph_objects as go

# Read data in from CSV
candles = pd.read_csv('./data/kucoin/BTC-USDT.csv')

# Parse timestamps into readable datetimes
candles['Date'] = pd.to_datetime(candles['Date'], unit='ms')

#  Create plotly figure
fig = go.Figure(data=[go.Candlestick(
                x=candles['Date'],
                open=candles['Open'],
                high=candles['High'],
                low=candles['Low'],
                close=candles['Close']
)])

# Remove rangeslider
fig.update_layout(xaxis_rangeslider_visible=False)
fig.show()