# Import the flask app from the server directory and run using this file
from server import create_app

if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)